package info.textgrid.lab.revmenu.handlers;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.util.ModelUtil;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.tgcrud.client.CrudClientUtilities;
import info.textgrid.lab.navigator.NaviView;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.xml.ws.Holder;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;

public class CreateNewRevisionHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		List<URI> uris = new ArrayList<URI>();
		List<TextGridObject> tgos = new ArrayList<TextGridObject>();

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			for(Object obj : ((IStructuredSelection) selection).toList()) {
				TextGridObject tgo = AdapterUtils.getAdapter(obj, TextGridObject.class);
				uris.add(tgo.getURI());
				tgos.add(tgo);
			}
		}
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		boolean doIt = MessageDialog.openConfirm(
				window.getShell(),
				"Create new revision",
				"Creating new revision of " + uris.toString());

		if(doIt) {
			try {
				createNewRevision(tgos, null);
			} catch (CoreException e) {
				throw new ExecutionException("Error creating new revision(s) of " + uris, e);
			}
		} else {
			System.out.println("chancelled");
		}

		return null;
	}

	private void createNewRevision(List<TextGridObject> tgos, IProgressMonitor monitor) throws CoreException {

		TGCrudService tgcrud = CrudClientUtilities.getCrudServiceStub();

		for(TextGridObject tgo : tgos) {

			if (monitor == null) {
				monitor = new NullProgressMonitor();
			}
			monitor.beginTask(NLS.bind("Creating new Revision {0}", tgo.getURI()), 6);

			String sid = RBACSession.getInstance().getSID(true);
			String uri = tgo.getURI().toString();

	        Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
	        Holder<DataHandler> dataHolder = new Holder<DataHandler>();

			ModelUtil.checkNonUIThread("Creating new revision of {0} from an UI-Thread", uri);

			/*if (tgo.isComplete()) {// TG-576
				tgo.assertPermission(ITextGridPermission.CREATE, "Access denied: You are not allowed to create new revision off{0}",
						tgo);
			}*/

			try {

				tgcrud.read(sid, "", uri, metadataHolder, dataHolder);
		        tgcrud.create(sid, "", uri, true,
	                    tgo.getProject(), metadataHolder, dataHolder.value);
				monitor.worked(1);
				//logServiceCall("... delete({0}) succeeded", uri);
				monitor.worked(5);
			} catch (ObjectNotFoundFault | MetadataParseFault | IoFault | ProtocolNotImplementedFault | AuthFault | CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			monitor.done();
		}

		UIJob refreshNavi = new UIJob("create new revision - refresh navigator") {
			@SuppressWarnings("static-access")
			@Override
			public IStatus runInUIThread(IProgressMonitor arg0) {
				NaviView navi = (NaviView) PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.findView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
				navi.refreshNavigator();
				return new Status(IStatus.OK, "info.textgrid.lab.revmenu",
						"create new revision - refresh navigator");
			}
		};
		refreshNavi.schedule();


	}
}
